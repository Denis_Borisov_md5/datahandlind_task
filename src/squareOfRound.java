public class squareOfRound
{
    public static void main(String[] args)
    {
        int radius = 3;
        double square = Math.PI * (radius * radius);
        String formattedDouble = String.format("%.50f", square);
        System.out.println("Square: " + formattedDouble);
    }
}

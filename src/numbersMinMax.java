public class numbersMinMax
{
    public static int max(int a, int b)
    {
        return(a + b + Math.abs(a - b))/2;
    }

    public static int min(int a, int b)
    {
        return(a + b - Math.abs(a - b))/2;
    }

    public static void main(String[] args)
    {
        int x = 5, y = 7, z = 10;
        int max = max (max(x,y),z);
        int min = min (min(x,y),z);

        System.out.println("Maximum num is: " + max);
        System.out.println("Minimum num is: " + min);
    }
}
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DateCompare
{
    public static void main(String[] args)
    {
        String date1 = "31.12.2018";
        String date2 = "12.12.2018";

        System.out.println("Date 1: " + date1);
        System.out.println("Date 2: " + date2);

        /*LocalDate date1 = LocalDate.of(2018,12,12);
        LocalDate date2 = LocalDate.of(2018,12,31);*/

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate datef1 = LocalDate.parse(date1, formatter);
        LocalDate datef2 = LocalDate.parse(date2, formatter);

        Period periodBetweenDates = Period.between(datef1, datef2);

        System.out.println("Period between dates in days: " + Math.abs(periodBetweenDates.getDays()));
    }
}

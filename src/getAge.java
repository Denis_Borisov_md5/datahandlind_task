import java.time.LocalDateTime;

public class getAge
{
    public static void main(String[] args)
    {
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime dateOfBirth = LocalDateTime.of(1982,10,01,00,00,00);

        long ageY = currentDateTime.getYear() - dateOfBirth.getYear();
        long ageM = currentDateTime.getMonthValue() - dateOfBirth.getMonthValue();
        long ageD = currentDateTime.getDayOfMonth() - dateOfBirth.getDayOfMonth();
        long ageH = currentDateTime.getHour() - dateOfBirth.getHour();
        long ageMn = currentDateTime.getMinute() - dateOfBirth.getMinute();
        long ageSec = currentDateTime.getSecond() - dateOfBirth.getSecond();

        System.out.println("Current Date and Time: " + currentDateTime);
        System.out.println("Date of Birth: " + dateOfBirth);
        System.out.println("Age is: "+ageY+" years, "+ageM+" month, "+ageD+" days, "+ageH+" hours, "+ageMn+" minutes, "+ageSec+" seconds.");
    }
}

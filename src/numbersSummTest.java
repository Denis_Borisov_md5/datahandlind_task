import java.util.Arrays;

public class numbersSummTest
{
    public static void main(String[] args)
    {
        String text = "0.1 0.15 0.25";
        double[] numbers = Arrays.stream(text.split("\\s"))
                .mapToDouble(Double::parseDouble)
                .toArray();
        for (int i = 0; i < 3; i++)
            System.out.println("Number # " + (i+1) + ": " + numbers[i]);
        if ((numbers[0]+numbers[1])==numbers[2])
            System.out.println("3rd number is sum of 1st and 2nd number");
        else
            System.out.println("Sum of 1st and 2nd number makes not 3rd number");
    }
}

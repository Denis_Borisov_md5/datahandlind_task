import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class dateConverting
{
    public static void main(String[] args)
    {
       String date1 = "Wednesday, Aug 10, 2016 12:10:56 PM";

        System.out.println("Date 1: " + date1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);

        LocalDate datef1 = LocalDate.parse(date1, formatter);

        System.out.println("Converted Date 1: " + datef1);
    }
}
